#!/bin/sh

 # if any lines fail, return/display error 
set -e

# envsubst takes in the template, 
# populates env vars 
# and outputs to default nginx conf directory
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# starts nginx with daemon off (ensures it runs in the foreground (not background))
# ensures nginx logs are printed to docker logs
nginx -g 'daemon off;'