# Recipie App API Proxy

NGINX proxy for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Default 8000
* `APP_HOST` - Hostname of app to forward requests to (default: app)
* `APP_PORT` - Port of the app to forward requests to (default: 9000)